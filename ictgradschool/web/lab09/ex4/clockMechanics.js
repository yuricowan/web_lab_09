/**
 * Created by ycow194 on 5/04/2017.
 */
var tick;

function clock() {
    var date = new Date();

    var time = date.toLocaleTimeString();

    document.getElementById("backToTheFuture").innerHTML = time;

}
function stopTimer() {
    clearInterval(tick);
}

function resumeTimer() {
    tick = setInterval(clock, 1000);
}