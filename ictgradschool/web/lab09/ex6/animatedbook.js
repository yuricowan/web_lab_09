/**
 * Created by ycow194 on 11/04/2017.
 */

var pages = document.getElementsByClassName("page");


function onClickerino(i) {
    var thisfunc = function () {
        pages[i].classList.add("pageAnimation");
    };
    console.log(thisfunc);
    return thisfunc;
}

function onfinished(i) {
    var otherfunc = function () {
        if (i <= pages.length) {
            pages[i + 1].style.zIndex = 1;
        }
    };
    return otherfunc;
}

for (var i = 0; i < pages.length; i++) {
    pages[i].onclick = onClickerino(i);
    pages[i].addEventListener("animationend", onfinished(i));
}
