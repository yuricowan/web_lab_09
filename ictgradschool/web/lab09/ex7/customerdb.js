var customers = [
    {"name": "Peter Jackson", "gender": "male", "year_born": 1961, "joined": "1997", "num_hires": 17000},

    {"name": "Jane Campion", "gender": "female", "year_born": 1954, "joined": "1980", "num_hires": 30000},

    {"name": "Roger Donaldson", "gender": "male", "year_born": 1945, "joined": "1980", "num_hires": 12000},

    {"name": "Temuera Morrison", "gender": "male", "year_born": 1960, "joined": "1995", "num_hires": 15500},

    {"name": "Russell Crowe", "gender": "male", "year_born": 1964, "joined": "1990", "num_hires": 10000},

    {"name": "Lucy Lawless", "gender": "female", "year_born": 1968, "joined": "1995", "num_hires": 5000},

    {"name": "Michael Hurst", "gender": "male", "year_born": 1957, "joined": "2000", "num_hires": 15000},

    {"name": "Andrew Niccol", "gender": "male", "year_born": 1964, "joined": "1997", "num_hires": 3500},

    {"name": "Kiri Te Kanawa", "gender": "female", "year_born": 1944, "joined": "1997", "num_hires": 500},

    {"name": "Lorde", "gender": "female", "year_born": 1996, "joined": "2010", "num_hires": 1000},

    {"name": "Scribe", "gender": "male", "year_born": 1979, "joined": "2000", "num_hires": 5000},

    {"name": "Kimbra", "gender": "female", "year_born": 1990, "joined": "2005", "num_hires": 7000},

    {"name": "Neil Finn", "gender": "male", "year_born": 1958, "joined": "1985", "num_hires": 6000},

    {"name": "Anika Moa", "gender": "female", "year_born": 1980, "joined": "2000", "num_hires": 700},

    {"name": "Bic Runga", "gender": "female", "year_born": 1976, "joined": "1995", "num_hires": 5000},

    {"name": "Ernest Rutherford", "gender": "male", "year_born": 1871, "joined": "1930", "num_hires": 4200},

    {"name": "Kate Sheppard", "gender": "female", "year_born": 1847, "joined": "1930", "num_hires": 1000},

    {"name": "Apirana Turupa Ngata", "gender": "male", "year_born": 1874, "joined": "1920", "num_hires": 3500},

    {"name": "Edmund Hillary", "gender": "male", "year_born": 1919, "joined": "1955", "num_hires": 10000},

    {"name": "Katherine Mansfield", "gender": "female", "year_born": 1888, "joined": "1920", "num_hires": 2000},

    {"name": "Margaret Mahy", "gender": "female", "year_born": 1936, "joined": "1985", "num_hires": 5000},

    {"name": "John Key", "gender": "male", "year_born": 1961, "joined": "1990", "num_hires": 20000},

    {"name": "Sonny Bill Williams", "gender": "male", "year_born": 1985, "joined": "1995", "num_hires": 15000},

    {"name": "Dan Carter", "gender": "male", "year_born": 1982, "joined": "1990", "num_hires": 20000},

    {"name": "Bernice Mene", "gender": "female", "year_born": 1975, "joined": "1990", "num_hires": 30000}
];

var table = document.getElementById("tableData");

var customerLength = customers.length;

var maleCounter = 0;
var femaleCounter = 0;

var bornEarly = 0;
var bornMiddle = 0;
var bornLate = 0;

var bronzeStatus = 0;
var silverStatus = 0;
var goldStatus = 0;


for (var i = 0; i < customerLength; i++) {

    //Sorting gender
    if (customers[i].gender == "male") {
        maleCounter++;
    }
    else if (customers[i].gender == "female") {
        femaleCounter++;
    }

    //Sorting age of people
    if (2017 - customers[i].year_born <= 30) {
        bornEarly++;
    }
    else if ((2017 - customers[i].year_born) > 30 && (2017 - customers[i].year_born <= 64)) {
        bornMiddle++;
    }
    if (2017 - customers[i].year_born >= 65) {
        bornLate++;
    }

    //Sorting status of how many video people hire per week
    var status = (customers[i].num_hires / (((2017 - customers[i].joined) * 52)));

    if (status < 1) {
        bronzeStatus++;
    }
    else if (status >= 1 && status <= 4) {
        silverStatus++;
    }
    else if (status > 4) {
        goldStatus++;
    }

}


for (var i = 0; i < customers.length; i++) {
    var p = document.createElement("p");
    console.log(i);
    console.log(document.getElementById('container').append(p));
    p.innerHTML = "Name: " + customers[i].name + " Gender: " + customers[i].gender + " Year Born:" + customers[i].year_born + " Year-joined:" + customers[i].joined + " Number of hires: " + customers[i].num_hires;
}

var additionalInfo = document.createElement("p");
document.getElementById('container').append(additionalInfo);
additionalInfo.innerHTML = "" + "Male counter: " + maleCounter + " Female counter: " + femaleCounter + " Born-early: " + bornEarly + " Born-middle: " + bornMiddle + " Born-late: " + bornLate + " Bronze-status: " + bronzeStatus + " Silver-status: " + silverStatus + " Gold-status: " + goldStatus;




