/**
 * Created by ycow194 on 5/04/2017.
 */
var baubles = document.getElementsByClassName("bauble");
var animate = "animation: bounce 3s; animation-fill-mode: forwards;";

function onMouseOver(i) {
    var func = function() {
        baubles[i].style.cssText = animate;
        baubles[i].title = "";
    };
    console.log(func);
    return func;
}

for (var i = 0; i < baubles.length; i++) {
    baubles[i].onmouseover = onMouseOver(i);
    // baubles[i].onclick = onMouseOver(i); cant click without hovering ;)
}